<?php

namespace Drupal\search_api_coveo\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Processor\ConfigurablePropertyBase;

/**
 * Defines a "calculated value" property.
 *
 * The callable referenced in this property must be a static method in a class.
 * The method must be marked with the TrustedCallback Attribute.
 *
 * The method signature should be:
 * public static function someCallable(Type $object) {}
 *
 * Where Type is the type of object being indexed.
 *
 * @see \Drupal\Core\TypedData\ComplexDataInterface
 * @see \Drupal\search_api\Item\ItemInterface::getOriginalObject()
 * @see \Drupal\Core\Security\Attribute\TrustedCallback
 */
class CoveoCalculatedValueProperty extends ConfigurablePropertyBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'callable' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(FieldInterface $field, array $form, FormStateInterface $form_state) {
    $configuration = $field->getConfiguration();
    $form['callable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('A callable'),
      '#description' => $this->t('Enter a callable method as a string: MyClass::myCallbackMethod. The method must be marked with the TrustedCallback Attribute.'),
      '#default_value' => $configuration['callable'] ?? '',
    ];
    return $form;
  }

}
