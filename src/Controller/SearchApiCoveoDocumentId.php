<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for Search API Coveo Document ID redirects.
 */
class SearchApiCoveoDocumentId implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Redirect a Coveo Document ID which is a URI to the entity's URI.
   *
   * @param string $index_id
   *   The Search API index.
   * @param string $entity_type
   *   The entity type and entity type id.
   * @param string $entity_id
   *   The entity id and langcode.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to an entity's URI.
   */
  public function index($index_id, $entity_type, $entity_id) {
    // Make sure entity type and id follow the item id naming convention.
    // (entity:node/100003:en)
    if (strpos($entity_type, ':') === FALSE
      || strpos($entity_id, ':') === FALSE) {
      throw new NotFoundHttpException();
    }

    // Get entity type id, entity id, and langcode.
    [, $entity_type_id] = explode(':', $entity_type);
    [$entity_id, $langcode] = explode(':', $entity_id);

    // Make sure the entity type id exists, else throw a not found exception.
    try {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
    }
    catch (\Exception $exception) {
      throw new NotFoundHttpException();
    }

    // Make sure the search API index exists.
    $index = $this->entityTypeManager->getStorage('search_api_index')->load($index_id);
    if (!$index) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $storage->load($entity_id);
    // Make sure the entity translation exists, else throw a not
    // found exception.
    if (!$entity->hasTranslation($langcode)) {
      throw new NotFoundHttpException();
    }

    // Get the translated entity's absolute URI.
    $entity = $entity->getTranslation($langcode);
    $options = [
      'absolute' => TRUE,
      'language' => $entity->language(),
    ];
    $uri = $entity->toUrl('canonical', $options)->toString();

    return new RedirectResponse($uri, 301);
  }

}
