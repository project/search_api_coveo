<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\search_api_coveo_keys\RepositoryManagerInterface;

/**
 * Implements a credential service to support multiple providers.
 */
interface CredentialProviderInterface {

  /**
   * Provides a means to our services.yml file to conditionally inject service.
   *
   * @param \Drupal\search_api_coveo_keys\RepositoryManagerInterface $repository
   *   The injected service, if it exists.
   */
  public function setKeyRepository(RepositoryManagerInterface $repository): void;

  /**
   * Detects if key module service was injected.
   *
   * @return bool
   *   True if the KeyRepository is present.
   */
  public function additionalProviders(): bool;

  /**
   * Get the provided credentials.
   *
   * @param array $config
   *   An array of search_api_coveo plugin configuration.
   *
   * @return array|string
   *   The value of the configured key.
   */
  public function getCredentials(array $config): array|string;

}
