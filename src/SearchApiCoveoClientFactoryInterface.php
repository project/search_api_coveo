<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;

/**
 * A factory class to provide clients to the backend.
 */
interface SearchApiCoveoClientFactoryInterface {

  /**
   * Create a SearchApiCoveoClient.
   */
  public function getClient(string $sourceID, string $organizationID, string $apiKey, CoveoPushApiEndpoint $host, string $serverId): SearchApiCoveoClientInterface;

}
