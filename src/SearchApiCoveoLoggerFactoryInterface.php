<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

/**
 * This factory looks up the log threshold and returns a logging channel.
 */
interface SearchApiCoveoLoggerFactoryInterface {

  /**
   * Get a logger from our memory cache or instantiate and cache.
   *
   * @param string $serverId
   *   A search API server ID.
   *
   * @return \Drupal\search_api_coveo\SearchApiCoveoLoggerChannel
   *   A configured channel.
   */
  public function get(string $serverId): SearchApiCoveoLoggerChannel;

}
