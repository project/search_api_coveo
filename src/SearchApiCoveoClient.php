<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfo;
use Drupal\search_api_coveo\DataStructure\CoveoFieldInfoSet;
use Drupal\search_api_coveo\DataStructure\CoveoFieldOrigin;
use Drupal\search_api_coveo\DataStructure\CoveoPushStatus;
use Drupal\search_api_coveo\DataStructure\CoveoS3FileData;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Entry point in the PHP API.
 */
class SearchApiCoveoClient implements SearchApiCoveoClientInterface {
  use LoggerTrait;
  use LogMessageTrait;
  use StringTranslationTrait;

  /**
   * Our max request size in bytes to the Coveo Push API.
   *
   * Set to 150 * 1024 * 1024.
   */
  const MAX_PUSH_BYTES = 157286400;

  /**
   * A flag for batch initialization.
   *
   * @var bool
   */
  protected bool $batchStarted = FALSE;

  /**
   * A Unix timestamp in microseconds.
   *
   * @var float
   */
  protected float $startTime;

  /**
   * The current size of the data in the queue data.
   *
   * @var int
   */
  protected int $currentPushSize = 0;

  /**
   * JSON encoded documents waiting to be pushed in the current batch.
   *
   * @var string[]
   */
  protected array $pushQueue = [];

  /**
   * Coveo Search initialization.
   *
   * @param \Drupal\search_api_coveo\SearchApiCoveoClientContext $context
   *   The current context.
   *   Logger.
   * @param \Drupal\Core\Http\ClientFactory $clientFactory
   *   The http client factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\search_api_coveo\SearchApiCoveoLoggerChannel $logger
   *   Injected SearchApiCoveoLoggerFactory provided logger.
   */
  public function __construct(
    protected SearchApiCoveoClientContext $context,
    protected ClientFactory $clientFactory,
    protected MessengerInterface $messenger,
    SearchApiCoveoLoggerChannel $logger,
  ) {
    $this->setLogger($logger);
  }

  /**
   * {@inheritdoc}
   */
  public function startBatch(): bool {
    if ($this->batchStarted) {
      return TRUE;
    }
    try {
      $this->setStartTime();
      $this->batchStarted = $this->updateCoveoStatus(CoveoPushStatus::Rebuild);
      if ($this->batchStarted === FALSE) {
        $variables['%start_ordering_id'] = (string) $this->startTime;
        $this->logMessage('Failed to start batch %start_ordering_id.', $variables);
      }
    }
    catch (\Exception $ex) {
      $this->logException($ex);
      $this->batchStarted = FALSE;
    }
    return $this->batchStarted;
  }

  /**
   * {@inheritdoc}
   */
  public function addDocument(CoveoDocument $document): bool {
    $result = TRUE;
    try {
      $result = $this->addCoveoDocument($document);
      if ($result === FALSE) {
        $variables['%document_id'] = $document->documentId;
        $this->logMessage('Add coveo document to batch with id %document_id failed.', $variables);
      }
    }
    catch (\Exception $ex) {
      $this->logException($ex);
      return FALSE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function endBatch(): bool {
    try {
      $succeeded = $this->pushCoveoBatch() && $this->updateCoveoStatus(CoveoPushStatus::Idle);
      if ($succeeded === FALSE) {
        $variables['%start_ordering_id'] = (string) $this->startTime;
        $this->logMessage('Failed to end batch %start_ordering_id.', $variables);
      }
      else {
        $this->batchStarted = FALSE;
      }
    }
    catch (\Exception $ex) {
      $this->logException($ex);
      return FALSE;
    }
    return $succeeded;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOlderThan(): bool {
    try {
      $this->setStartTime();
      $result = $this->deleteCoveoByTime($this->startTime);
      if ($result === FALSE) {
        $variables['%start_ordering_id'] = $this->startTime;
        $this->logMessage('Delete documents older than %start_ordering_id failed.', $variables);
      }
    }
    catch (\Exception $e) {
      $this->logException($e);
      return FALSE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItem(string $documentId): bool {
    try {
      $result = $this->deleteCoveoDocument($documentId);
      if ($result === FALSE) {
        $variables['%documentId'] = $documentId;
        $this->logMessage('Remove single document with Id %documentId failed.', $variables);
      }
    }
    catch (\Exception $ex) {
      $this->logException($ex);
      return FALSE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizations(): array {
    return $this->get(
      path: '',
      options: ['base_uri' => $this->context->organizationBase]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createFields(CoveoFieldInfoSet $bulk): bool {
    if ($bulk->isEmpty()) {
      return FALSE;
    }
    $path = SearchApiCoveoClientContext::FIELD_BATCH . '/create';
    $options['json'] = $bulk->toArray();
    return $this->post($path, $options) !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUserFields(): CoveoFieldInfoSet {
    $parameters = [
      'filters' => [
        'facet' => "ALL",
        "sort" => "ALL",
      ],
    ];

    return $this->listFields($parameters, CoveoFieldOrigin::User);
  }

  /**
   * {@inheritdoc}
   */
  public function listFields(array $parameters, CoveoFieldOrigin $origin = CoveoFieldOrigin::User): CoveoFieldInfoSet {
    $defaults = [
      'filters' => [
        'facet' => 'ALL',
        'sort' => 'ALL',
      ],
    ];
    $originFilter = [
      'filters' => [
        'origin' => $origin->value,
      ],
    ];
    $parameters = NestedArray::mergeDeep($defaults, $parameters, $originFilter);

    $current_page = 0;
    $path = SearchApiCoveoClientContext::FIELD_LIST;
    $fieldsFound = new CoveoFieldInfoSet([]);
    do {
      $parameters['page'] = $current_page;
      $options['json'] = $parameters;
      $field_data = $this->post($path, $options);
      $total_pages = $total_pages ?? $field_data['totalPages'] ?? 1;
      $last_page = $last_page ?? $total_pages - 1;
      if (is_array($field_data['items'])) {
        foreach ($field_data['items'] as $info) {
          $fieldsFound->add(new CoveoFieldInfo(...$info));
        }
      }
      $current_page++;
    } while ($current_page <= $last_page);

    return $fieldsFound;
  }

  /**
   * {@inheritdoc}
   */
  public function loadField(string $field_id): CoveoFieldInfo|false {
    if (empty($field_id)) {
      return FALSE;
    }
    try {
      $result = $this->get(
        path: SearchApiCoveoClientContext::FIELD_LOAD . '/' . $field_id,
        options: []
      );
      if (!empty($result)) {
        return new CoveoFieldInfo(...$result);
      }
    }
    catch (\Exception $e) {
      $this->logException($e);
      $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
      return FALSE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadSystemFields(): CoveoFieldInfoSet {
    $parameters = [
      'filters' => [
        'facet' => "ALL",
        "sort" => "ALL",
      ],
    ];

    return $this->listFields($parameters, CoveoFieldOrigin::System);
  }

  /**
   * {@inheritdoc}
   */
  public function updateFields(CoveoFieldInfoSet $bulk): array|false {
    if ($bulk->isEmpty()) {
      return FALSE;
    }
    $path = SearchApiCoveoClientContext::FIELD_BATCH . '/update';
    $options['json'] = $bulk->toArray();
    return $this->put($path, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFields(CoveoFieldInfoSet $bulk): bool {
    if ($bulk->isEmpty()) {
      return FALSE;
    }
    $options['query'] = ['fields' => implode(',', $bulk->getFieldIds())];
    $path = SearchApiCoveoClientContext::FIELD_BATCH . '/delete';
    return $this->delete($path, $options) !== FALSE;
  }

  /* ====================================== */
  /* Internal Coveo Push API helper methods */
  /* ====================================== */

  /**
   * Set the start time to the current time.
   */
  protected function setStartTime(): void {
    $this->startTime = round((microtime(TRUE) * 1000), 0);
  }

  /**
   * Update Coveo index status.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoPushStatus $status
   *   The status to set.
   *
   * @return bool
   *   Status post successful.
   */
  protected function updateCoveoStatus(CoveoPushStatus $status): bool {
    $options = [
      'base_uri' => $this->context->pushBase,
      'query' => ['statusType' => $status->value],
    ];

    return $this->post(
      path: SearchApiCoveoClientContext::PUSH_STATUS,
      options: $options) !== FALSE;
  }

  /**
   * Adds a document to the Push queue.
   *
   * If adding the document would cause the queue to exceed self::MAX_PUSH_BYTES
   * then trigger the push and then add the document.
   *
   * @param \Drupal\search_api_coveo\DataStructure\CoveoDocument $document
   *   The document to add.
   *
   * @return bool
   *   Document added successfully.
   */
  protected function addCoveoDocument(CoveoDocument $document): bool {
    $json = json_encode($document);
    if (!is_string($json)) {
      $this->logger->error('Document: ' . $document->documentId . ' failed to encode as JSON in ::addCoveoDocument');
      return FALSE;
    }
    $size = strlen($json) + 1;
    // Check document size.
    if ($size > self::MAX_PUSH_BYTES) {
      $this->logger->error("Document: " . $document->documentId . " can\'t be larger than " . self::MAX_PUSH_BYTES . " bytes in size.");
      return FALSE;
    }
    else {
      $this->logger->debug("Document: " . $document->documentId . " Current size: " . $this->currentPushSize . " vs max: " . self::MAX_PUSH_BYTES);
    }
    // Check batch size for upload.
    if ($this->currentPushSize + $size > self::MAX_PUSH_BYTES - count($this->pushQueue)) {
      // This document would cause the batch to exceed the size limit.
      $this->logger->debug('Uploading the batch because adding ' . $document->documentId . ' would exceeded the max size.');
      $this->startBatch();
      $this->pushCoveoBatch();
    }
    $this->currentPushSize += $size;
    array_push($this->pushQueue, $document->toArray());
    $this->logger->debug('Added ' . $document->documentId . ' to the push queue.');
    return TRUE;
  }

  /**
   * Push a batch to Coveo.
   *
   * @return bool
   *   Push succeeded.
   */
  protected function pushCoveoBatch():bool {
    if (empty($this->pushQueue)) {
      $this->logger->info('pushCoveoBatch called on an empty queue.');
      return TRUE;
    }
    $s3Data = $this->getCoveoS3FileData();
    if ($s3Data instanceof CoveoS3FileData) {
      $empty = [
        'base_uri' => NULL,
        'headers' => NULL,
      ];
      // Get the client directly to remove the usual defaults.
      $httpClient = $this->getHttpClient($empty);
      // Assemble the request.
      $options = [
        'json' => ['addOrUpdate' => $this->pushQueue],
        'headers' => $s3Data->requiredHeaders,
      ];
      try {
        $httpClient->put($s3Data->uploadUri, $options);
        $upload = TRUE;
      }
      catch (GuzzleException $e) {
        $this->logException($e);
        $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
        $upload = FALSE;
      }
      // Notify Coveo to process the batch by sending the S3 File ID.
      $notify_options = [
        'base_uri' => $this->context->pushBase,
        'query' => ['fileId' => $s3Data->fileId, 'orderingId' => $this->startTime],
      ];
      $success = $upload && $this->put(SearchApiCoveoClientContext::PUSH_BATCH, $notify_options) !== FALSE;
      if ($success) {
        $this->pushQueue = [];
        $this->currentPushSize = 0;
      }
      return $success;
    }
    return FALSE;
  }

  /**
   * Initiate the creation of an S3 container to receive the batch.
   *
   * @return \Drupal\search_api_coveo\DataStructure\CoveoS3FileData|false
   *   If successful, the necessary data to use the S3 container.
   */
  protected function getCoveoS3FileData(): CoveoS3FileData|false {
    $options = [
      'base_uri' => $this->context->containerBase,
    ];
    $result = $this->post('files', $options);
    if ($result !== FALSE) {
      return new CoveoS3FileData(...$result);
    }
    return FALSE;
  }

  /**
   * Delete all documents before a given time.
   *
   * @param float $time
   *   The timestamp.
   *
   * @return bool
   *   Deletion succeeded.
   */
  protected function deleteCoveoByTime(float $time): bool {
    $options = [
      'base_uri' => $this->context->pushBase,
      'query' => ['orderingId' => $time],
    ];
    $result = $this->delete(SearchApiCoveoClientContext::PUSH_DELETE_TIME, $options);
    return $result !== FALSE;
  }

  /**
   * Delete a document by ID using the Coveo Push API.
   *
   * @param string $documentId
   *   The id.
   *
   * @return bool
   *   If the deletion succeeded.
   */
  protected function deleteCoveoDocument(string $documentId): bool {
    $setRebuild = $this->updateCoveoStatus(CoveoPushStatus::Rebuild);
    $options = [
      'base_uri' => $this->context->pushBase,
      'query' => ['documentId' => $documentId],
    ];
    $result = $this->delete(SearchApiCoveoClientContext::PUSH_DELETE_ITEM, $options);
    $setIdle = $this->updateCoveoStatus(CoveoPushStatus::Idle);
    return $setRebuild && $setIdle && $result !== FALSE;
  }

  /* ==================================== */
  /* Internal HTTP request helper methods */
  /* ==================================== */

  /**
   * Configures a Guzzle client.
   *
   * Set from $this->context:
   * - base_uri: Pass another value on this key to replace the default base uri.
   * - headers: Pass additional headers within an array and they will merge
   *   with the defaults.
   *
   * @return \GuzzleHttp\Client
   *   A configured client.
   */
  protected function getHttpClient(array $options = []): Client {
    $default_options = [
      'base_uri' => $this->context->fieldBase,
      'headers' => $this->context->defaultHeaders,
    ];
    $request_options = NestedArray::mergeDeep($default_options, $options);
    return $this->clientFactory->fromOptions($request_options);
  }

  /**
   * Executes a GET request on the requested path.
   *
   * @param string $path
   *   Additional path segment.
   * @param array $options
   *   Request options to pass to the http client. Optional.
   *
   * @return array|false
   *   A decoded JSON response from the endpoint.
   */
  protected function get(string $path, array $options): array|false {
    $http_client = $this->getHttpClient($options);
    try {
      $response = $http_client->get($path);
      $body = json_decode($response->getBody()->getContents(), TRUE);
      return $body ?? [];
    }
    catch (GuzzleException $e) {
      $this->logException($e);
      $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
      return FALSE;
    }
  }

  /**
   * Executes a POST request on the requested path.
   *
   * @param string $path
   *   Additional path segment.
   * @param array $options
   *   Additional http client options, including request options.
   *
   *   For POST requests, see especially
   *   https://docs.guzzlephp.org/en/stable/request-options.html#body.
   *
   * @return array|false
   *   A decoded JSON response from the endpoint.
   */
  protected function post(string $path, array $options): array|false {
    $http_client = $this->getHttpClient($options);
    try {
      $response = $http_client->post($path);
      $body = json_decode($response->getBody()->getContents(), TRUE);
      return $body ?? [];
    }
    catch (GuzzleException $e) {
      $this->logException($e);
      $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
      return FALSE;
    }
  }

  /**
   * Executes a PUT request on the requested path.
   *
   * @param string $path
   *   Additional path segment.
   * @param array $options
   *   Additional http client options, including request options.
   *
   *   For PUT requests, see especially
   *   https://docs.guzzlephp.org/en/stable/request-options.html#body.
   *
   * @return array|false
   *   A decoded JSON response from the endpoint.
   */
  protected function put(string $path, array $options): array|false {
    $http_client = $this->getHttpClient($options);
    try {
      $response = $http_client->put($path);
      $body = json_decode($response->getBody()->getContents(), TRUE);
      return $body ?? [];
    }
    catch (GuzzleException $e) {
      $this->logException($e);
      $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
      return FALSE;
    }
  }

  /**
   * Executes a DELETE request on the requested path.
   *
   * @param string $path
   *   Additional path segment.
   * @param array $options
   *   Additional http client options, including request options.
   *
   *   For PUT requests, see especially
   *   https://docs.guzzlephp.org/en/stable/request-options.html#body.
   *
   * @return array|false
   *   A decoded JSON response from the endpoint.
   */
  protected function delete(string $path, array $options): array|false {
    $http_client = $this->getHttpClient($options);
    try {
      $response = $http_client->delete($path);
      $body = json_decode($response->getBody()->getContents(), TRUE);
      return $body ?? [];
    }
    catch (GuzzleException $e) {
      $this->logException($e);
      $this->messenger->addError($this->t("Request failed. Check recent log messages for more details."));
      return FALSE;
    }
  }

}
