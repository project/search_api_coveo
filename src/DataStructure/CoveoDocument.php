<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

use Drupal\Component\Utility\UrlHelper;
use Psr\Log\LoggerInterface;

/**
 * Value Object to hold the data send to Coveo.
 *
 * @see https://docs.coveo.com/en/12/api-reference/push-api#tag/Item
 */
class CoveoDocument implements \JsonSerializable {

  const COMPRESSION_TYPE = 'ZLIB';

  const FILE_EXTENSION = '.html';

  /**
   * Reserved key names (case-insensitive) used in the Push API.
   */
  protected array $reservedKeys = [
    'author' => TRUE,
    'clickableuri' => TRUE,
    'compressedbinarydata' => TRUE,
    'compressedbinarydatafileid' => TRUE,
    'compressiontype' => TRUE,
    'data' => TRUE,
    'date' => TRUE,
    'documentid' => TRUE,
    'fileextension' => TRUE,
    'parentid' => TRUE,
    'permissions' => TRUE,
    'orderingid' => TRUE,
  ];

  /**
   * A date string .
   */
  public readonly string $date;

  /**
   * The permanent ID string.
   */
  public readonly string $permanentId;

  /**
   * A date string.
   */
  public readonly string $modifiedDate;

  /**
   * The clickable file extension for this document.
   */
  public readonly string $fileExtension;

  /**
   * The clickable URI for this document.
   */
  protected string $clickableUri;

  /**
   * If set, a string of base64 encoded data.
   */
  protected string $compressedBinaryData;

  /**
   * If set, a string declaring the compression scheme.
   */
  protected string $compressionType;

  /**
   * An array of additional key/value pairs.
   */
  protected array $metaData = [];

  /**
   * Coveo Document class constructor.
   *
   * @param string $documentId
   *   The unique identifier of the item. Must be the item URI.
   * @param \DateTimeInterface $date
   *   The date.
   * @param \DateTimeInterface $modifiedDate
   *   The modified date.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(public readonly string $documentId, \DateTimeInterface $date, \DateTimeInterface $modifiedDate, protected LoggerInterface $logger) {
    $this->permanentId = $this->hashId($documentId);
    $this->fileExtension = self::FILE_EXTENSION;
    $this->date = $date->format(\DateTimeInterface::ATOM);
    $this->modifiedDate = $modifiedDate->format(\DateTimeInterface::ATOM);
    // Validate required data.
    if (!UrlHelper::isValid($this->documentId, absolute: TRUE)) {
      throw new \ValueError('DocumentId is not a valid URL format [missing path]: ' . $this->documentId);
    }
    $this->logger->debug('Document ' . $this->documentId . ' is valid.');
  }

  /**
   * Hash document.
   *
   * @param string $documentId
   *   The document Id.
   *
   * @return string
   *   Hash value of $documentId.
   */
  protected function hashId($documentId): string {
    return hash('sha256', mb_convert_encoding($documentId, 'UTF-8', 'ISO-8859-1'));
  }

  /**
   * Setter for ::clickableUri.
   *
   * @param string $uri
   *   The URI to set.
   */
  public function setClickableUri(string $uri): void {
    $this->clickableUri = $this->clickableUri ?? $uri;
  }

  /**
   * Sets the CompressedBinaryData property.
   *
   * ZLIB compresses the string and then base64 encodes it.
   *
   * @param string $content
   *   Content to be pushed.
   */
  public function setContentAndZlibCompress(string $content): void {
    // Check if empty.
    if ($content === '') {
      $this->logger->error("setContentAndZlibCompress: value not set");
      return;
    }
    $compresseddata = gzcompress($content, 9);
    $encodeddata = base64_encode($compresseddata);

    $this->compressedBinaryData = $encodeddata;
    $this->compressionType = self::COMPRESSION_TYPE;
  }

  /**
   * Sets the metadata.
   *
   * @param string $key
   *   The key value to set.
   * @param mixed $value
   *   The value or object to set (str or list).
   */
  public function addMetadata(string $key, mixed $value) {
    // Check if empty.
    if ($key === '') {
      $this->logger->error("addMetadata: key not set");
      return;
    }

    // Check if in reserved keys.
    $lower = strtolower($key);
    if (isset($this->reservedKeys[$lower])) {
      $this->logger->warning("addMetadata: " . $key . " is a reserved field and cannot be set as metadata.");
      return;
    }

    // Check if empty.
    if ($value === '' || $value === NULL || $value === []) {
      $message = is_null($value) ? 'NULL' : 'an empty string or array';
      $this->logger->debug('addMetadata: value of ' . $message . ' not set for metadata key ' . $key);
      return;
    }
    else {
      $this->metaData[$lower] = $value;
    }
  }

  /**
   * Formats the value object as an array.
   *
   * Filters out null values from the resulting array.
   *
   * @return array
   *   The filtered array.
   */
  public function toArray(): array {
    $values = [
      'documentId' => $this->documentId,
      'date' => $this->date ?? NULL,
      'permanentId' => $this->permanentId ?? NULL,
      'modifiedDate' => $this->modifiedDate ?? NULL,
      'fileExtension' => $this->fileExtension ?? NULL,
      'clickableUri' => $this->clickableUri ?? NULL,
      'compressedBinaryData' => $this->compressedBinaryData ?? NULL,
      'compressionType' => $this->compressionType ?? NULL,
    ];
    $values = array_merge($values, $this->metaData);
    // Reduce the array to keys with explicit values.
    $not_null = fn($value) => !is_null($value);
    return array_filter($values, $not_null);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

}
