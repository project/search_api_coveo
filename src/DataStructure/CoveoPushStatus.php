<?php

declare(strict_types=1);

namespace Drupal\search_api_coveo\DataStructure;

/**
 * Provides the permitted values for Coveo status.
 */
enum CoveoPushStatus: string {

  case Incremental = 'INCREMENTAL';
  case Refresh = 'REFRESH';
  case Rebuild = 'REBUILD';
  case Idle = 'IDLE';
}
