<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\Plugin\search_api\processor\CoveoRegexReplace;
use Drupal\Tests\search_api\Unit\Processor\ProcessorTestTrait;
use Drupal\Tests\search_api\Unit\Processor\TestItemsTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group search_api_coveo
 */
final class CoveoRegexProcessorTest extends UnitTestCase {

  use ProcessorTestTrait;
  use TestItemsTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->setUpMockContainer();
    $patterns = [
      [
        'pattern' => '# (data-[a-z-_]+|class|id)="[^"]+"#',
        'replacement' => '',
      ],
      [
        'pattern' => '#foo|bar#',
        'replacement' => 'baz',
      ],
    ];
    $this->processor = new CoveoRegexReplace(
      configuration: [
        'patterns' => $patterns,
      ],
      plugin_id: 'coveo_regex_filter',
      plugin_definition: []
    );
  }

  /**
   * Tests preprocessing field values with script and style tags.
   *
   * @param string $passed_value
   *   The value that should be passed into process().
   * @param string $expected_value
   *   The expected processed value.
   *
   * @dataProvider providerRegexFilter
   */
  public function testReplacements(string $passed_value, string $expected_value): void {
    $this->invokeMethod('processFieldValue', [&$passed_value, 'text']);
    $this->assertEquals($expected_value, $passed_value);
  }

  /**
   * Data provider for testReplacements().
   *
   * @return array
   *   An array of test cases.
   */
  public static function providerRegexFilter(): array {
    return [
      ['<div data-test="the-data" ></div>', '<div ></div>'],
      ['foo bar zoom', 'baz baz zoom'],
      ['<div data-test="the-data" ></div> foo bar zoom', '<div ></div> baz baz zoom'],
    ];
  }

}
