<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\DataStructure\CoveoDocument;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group search_api_coveo
 */
final class CoveoDocumentTest extends UnitTestCase {

  /**
   * A document ID.
   */
  protected string $documentId = 'https://www.example.org/entity:node/100008:en';

  /**
   * The document canonical URI.
   */
  protected string $clickableUri = 'https://www.example.com/example';

  /**
   * A string of HTML.
   */
  protected string $renderedContent = '<article lang="en"><h2>Test</h2><p>Content</p></article>';

  /**
   * A sample set of metadata.
   */
  protected array $metaData = [
    'documentType' => 'WebPage',
    'sourceType' => 'Push',
    'test_changed' => 1638890355,
    'test_created' => 1312972499,
    'test_fulltype' => 'page',
    'test_index' => 'test',
    'test_language' => 'English',
    'test_nid' => 108,
    'test_result_address' => '',
    'test_result_authors' => [],
    'test_result_category' => '',
    'test_result_date' => '1312972499',
    'test_result_group' => '',
    'test_result_region_links' => NULL,
    'test_result_related_locations' => NULL,
    'test_result_summary' => 'This is a summary',
    'test_result_translations_links' => NULL,
    'test_type' => 'page',
    'title' => 'Example: Content',
    'hide_from_main_search' => FALSE,
    'test_result_image' => '',
    'test_patient_ed_diseases_taxonomy' => '',
  ];

  /**
   * An expected array of key/value pairs.
   */
  protected array $expected = [
    'documentId' => 'https://www.example.org/entity:node/100008:en',
    'permanentid' => 'afdd637a6714dc2b7b03487b33ffeb88311c58276dd2fecd7c2af4561fbb1b48',
    'clickableUri' => 'https://www.example.com/example',
    'compressedBinaryData' => 'eNqzSSwqyUzOSVXIScxLt1VKzVOys8kwsgtJLS6x0QcybArsnPPzSlLzgNwCOxt9qHI7ABdPEtc=',
    'compressionType' => 'ZLIB',
    'date' => 'see ::setup()',
    'modifiedDate' => 'see ::setup()',
    'fileExtension' => '.html',
    'documentType' => 'WebPage',
    'sourceType' => 'Push',
    'test_changed' => 1638890355,
    'test_created' => 1312972499,
    'test_fulltype' => 'page',
    'test_index' => 'test',
    'test_language' => 'English',
    'test_nid' => 108,
    'test_result_date' => '1312972499',
    'test_result_summary' => 'This is a summary',
    'test_type' => 'page',
    'title' => 'Example: Content',
    'hide_from_main_search' => FALSE,
  ];

  /**
   * A date.
   */
  protected \DateTime $date;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->date = new \DateTime();
    $this->expected['date'] = $this->date->format(\DateTimeInterface::ATOM);
    $this->expected['modifiedDate'] = $this->date->format(\DateTimeInterface::ATOM);
  }

  /**
   * Tests something.
   */
  public function testDocument(): void {
    $logger = $this->createMock('\Psr\Log\LoggerInterface');
    $document = new CoveoDocument($this->documentId, $this->date, $this->date, $logger);
    $document->setContentAndZlibCompress($this->renderedContent);
    $document->setClickableUri($this->clickableUri);
    foreach ($this->metaData as $key => $value) {
      $document->addMetadata($key, $value);
    }
    $result = $document->toArray();
    $this->assertEqualsCanonicalizing($this->expected, $result);
  }

}
