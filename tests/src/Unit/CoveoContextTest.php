<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_coveo\Unit;

use Drupal\search_api_coveo\DataStructure\CoveoPlatformEndpoint;
use Drupal\search_api_coveo\DataStructure\CoveoPushApiEndpoint;
use Drupal\search_api_coveo\SearchApiCoveoClientContext;
use Drupal\Tests\UnitTestCase;

/**
 * Test the logic in our context data object..
 *
 * @group search_api_coveo
 */
final class CoveoContextTest extends UnitTestCase {

  /**
   * Tests context values.
   */
  public function testContext(): void {
    $hippaContext = new SearchApiCoveoClientContext('org-id', 'source-id', 'api-key', CoveoPushApiEndpoint::Hipaa);
    $this->assertEquals('org-id', $hippaContext->organizationID);
    $this->assertEquals('source-id', $hippaContext->sourceID);
    $this->assertEquals('api-key', $hippaContext->apiKey);
    $this->assertStringStartsWith(CoveoPlatformEndpoint::Hipaa->value, $hippaContext->fieldBase);
    $this->assertStringStartsWith(CoveoPlatformEndpoint::Hipaa->value, $hippaContext->adminBase);
    $devContext = new SearchApiCoveoClientContext('org-id', 'source-id', 'api-key', CoveoPushApiEndpoint::Dev);
    $this->assertStringStartsWith(CoveoPlatformEndpoint::Default->value, $devContext->fieldBase);
    $this->assertStringStartsWith(CoveoPlatformEndpoint::Default->value, $devContext->adminBase);
  }

}
