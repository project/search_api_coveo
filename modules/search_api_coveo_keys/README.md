## INTRODUCTION

The Search API Coveo Keys module is an integration submodule.

The use case for this module is using the Key module store
confidential data for connecting to Coveo.

## REQUIREMENTS

This module requires the Key module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

After installing create and configure a key to hold the data.
See the annotation on the `\Drupal\search_api_coveo_keys\Plugin\KeyType\SearchApiCoveoKey`
plugin for the values that should be in your key.

Then in the configuration for your Coveo server within Search API, choose your key.
